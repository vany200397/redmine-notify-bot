<?php
    class RedmineNotifyTgBot extends TgBot
    {
        protected static array $STATUS_CODES = [
            1 => 'NEW',
            2=> 'IN_WORK',
            3 => 'RESOLVED',
            4 => 'FEEDBACK',
            9 => 'PAUSED'
        ];

        protected static array $PRIORITY_CODES = [
            1 => 'LOW',
            2 => 'NORMAL',
            3 => 'HIGH',
            4 => 'URGENT',
            5 => 'IMMEDIATE'
        ];

        protected static array $EMOJI = [
            'IN_WORK' => "\xF0\x9F\x92\xBC",
            'RESOLVED' => "\xE2\x9C\x85",
            'FEEDBACK' => "\xE2\x86\xA9",
            'NEW' => "\xE2\x9C\xA8",
            'HIGH' => "\xF0\x9F\x94\xBA",
            'URGENT' => "\xF0\x9F\x94\xA5",
            'IMMEDIATE' => "\xF0\x9F\x92\x80",
            'LOW' => "\xF0\x9F\x94\xBD",
            'NORMAL' => "\xE2\x86\x94",
            'PAUSED' => "\xE2\x8F\xB8"
        ];
 
        protected function processRequest()
        {
            parent::processRequest();

            $userMessage = (string)$this->userMessage;

            $lastTaskType = false;

            $lastTask = $this->getLastTask();
            if ($lastTask) {
                $lastTaskType = $lastTask['task'];
            }

            if ($lastTaskType && $lastTaskType != 'no') {
                
                if ($lastTaskType == 'wait') {
                    return;
                }
                
                if ($lastTaskType == 'get_redmine_url') {
                    $this->sendMessage('Подождите...');
                    $this->writeTask('wait');
                    $response = $this->getLastResponse();
                    if (!empty($response['code'])) {
                        $responseCode = $response['code'];
                        if ($responseCode == 200 || $responseCode == 302) {
                            $writeDataCheck = $this->writeData('user_settings/' . $this->userId,
                                    ['redmine_url' => $userMessage]
                            );
                            $writeTaskCheck = $this->writeTask('get_redmine_api_key');
                            if ($writeDataCheck && $writeTaskCheck) {
                                $this->sendPicture(IMAGES_PATH . 'redmine_api_key_example.png', 'Записано.' . PHP_EOL . 'Теперь укажите ваш Redmine api token. Он необходим для просмотра ваших задач.' . PHP_EOL . 'Для этого нужно зайти на свой аккаунт в Redmine и перейти в раздел "Моя учётная запись" (сверху справа).' . PHP_EOL . 'Затем скопировать API-ключ в правой части страницы.' . PHP_EOL . 'Если ключа нет, то его нужно для начала создать.');
                            }
                        }
                    }
                }
                elseif ($lastTaskType == 'get_redmine_api_key') {
                    $this->sendMessage('Подождите...');
                    $this->writeTask('wait');
                    $redmineUrl = $this->getData('user_settings/' . $this->userId, 'redmine_url');
                    $this->sendRequest($redmineUrl . 'issues.json', 'HEAD', '',
                        ['X-Redmine-API-Key: ' . $userMessage]
                    );
                    
                    $response = $this->getLastResponse();
                    $responseCode = $response['code'];
                    if ($responseCode != 200) {
                        $this->sendMessage('Неверный Api ключ, либо Redmine сервер временно недоступен.' .PHP_EOL . 'Проверьте ключ или повторите попытку позже.');
                    } else {

                        $userId = 0;

                        $this->sendRequest($redmineUrl . 'users/current.json', 'GET', '',
                            ['X-Redmine-API-Key: ' . $userMessage]
                        );

                        $userInfo = $this->getLastResponse();
                        $responseCode = $userInfo['code'];
                        if ($responseCode == 200) {
                            $userInfoDecode = json_decode($userInfo['data'], true);
                            if (!empty($userInfoDecode['user']['id'])) {
                                $userId = $userInfoDecode['user']['id'];
                            }
                        }

                        if (!$userId) {
                            $this->sendMessage('Не удалось получить ваш id в Redmine. Пожалуйста, повторите попытку позже.');
                        } else {
                            $checkWriteData = $this->writeData('user_settings/' . $this->userId,
                            [
                                'redmine_api_token' => $userMessage,
                                'redmine_user_id' => $userId
                            ]
                            );

                            if ($checkWriteData) {
                                $this->sendMessage('Api ключ успешно записан. Бот будет уведомлять о новых задачах каждые 15 минут.');
                                $this->sendAllUserTasks();
                                $this->sendHelp();
                                $this->writeTask('no');
                                return;
                            }
                        }
                        $this->writeTask('get_redmine_api_key');
                    }
                }

            } else {
                if ($userMessage == '/start') {
                    if (!$this->checkUserSettings()) {
                        $this->sendMessage('Данный бот будет уведомлять о новых задач в Redmine.' . PHP_EOL . 'Но сначала необходимо его настроить.');
                        $this->sendPicture(IMAGES_PATH . 'redmine_url_example.png', 'Для начала введите url-адрес вашего Redmine.');
                        $this->writeTask('get_redmine_url');
                    } else {
                        $this->sendMessage('Вы уже настроили бота. Если хотите изменить настройки введите команду /reconfig');
                    }
                } elseif ($userMessage == '/all') {
                    $this->sendMessage('Подождите...');
                    $this->writeTask('wait');
                    $this->sendAllUserTasks();
                    $this->writeTask('no');
                }
            }
        }

        protected function sendHelp($userId = 0)
        {
            if ($userId === 0) {
                $userId = $this->userId;
            }

            $this->sendMessage('Помощь.');
        }

        protected function setViewedTasks($tasks, $userId = 0)
        {
            if ($userId === 0) {
                $userId = $this->userId;
            }

            $arrToWrite = $this->getData('viewed_tasks/' . $userId);
            if (!$arrToWrite) {
                $arrToWrite = [];
            }

            foreach ($tasks as $taskItem) {
                $taskId = (string)$taskItem['id'];
                $arrToWrite[$taskId] = strtotime($taskItem['updated_on']);
            }
            unset($taskItem);

            return $this->writeData('viewed_tasks/' . $userId, $arrToWrite, false);
        }

        protected function getAllUserTasks($userId = 0)
        {
            if ($userId === 0) {
                $userId = $this->userId;
            }

            $userInfo = $this->getData('user_settings/' . $userId);
            $token = $userInfo['redmine_api_token'];
            $redmineUserId = $userInfo['redmine_user_id'];

            $remainder = 0;

            $redmineTasks = [];

            $page = 1;

            do {
                $requestUrl = sprintf('https://redmine.softmg.ru/issues.json?assigned_to_id=%s&limit=100&page=%s', $redmineUserId, $page);
                $this->sendRequest($requestUrl, 'GET', '',
                    ['X-Redmine-API-Key: ' . $token]
                );
                $response = $this->getLastResponse();
                if ($response['code'] == 200) {
                    $responseData = json_decode($response['data'], true);
                    if (!empty($responseData['issues'])) {
                        $redmineTasks = array_merge($redmineTasks, $responseData['issues']);
                    }
                    $totalCount = $responseData['total_count'];
                    $offset = $responseData['offset'];
                    $limit = $responseData['limit'];
                    $remainder = $totalCount - ($offset + $limit);
                }
                $page++;
            } while ($remainder >= 0);

            return $redmineTasks;
        }

        protected function sendTasks($redmineTasks, $userId = 0)
        {
            if ($userId === 0) {
                $userId = $this->userId;
            }

            $redmineUrl = $this->getData('user_settings/' . $userId, 'redmine_url');

            $sendData = [];

            for ($i = 0; $i < count($redmineTasks); $i++) {
                $taskToSendItem = $redmineTasks[$i];
                $link = '<a href="' . $redmineUrl . '/issues/' . $taskToSendItem['id'] . '">';
                $sendOneTaskData = $link . 'Задача №' . $taskToSendItem['id'] . '</a>' . PHP_EOL;
                $sendOneTaskData.= $link . 'Название: ' . $taskToSendItem['subject'] . '</a>' . PHP_EOL;
                $statusCode = $taskToSendItem['status']['id'];
                $statusEmoji = '';
                if (!empty(static::$STATUS_CODES[$statusCode])) {
                    $statusStringCode = static::$STATUS_CODES[$statusCode];
                    if (!empty(static::$EMOJI[$statusStringCode])) {
                        $statusEmoji = ' ' . static::$EMOJI[$statusStringCode];
                    }
                }
                $sendOneTaskData.= 'Статус: ' . $taskToSendItem['status']['name'] . $statusEmoji . PHP_EOL;
                $priorityCode = $taskToSendItem['priority']['id'];
                $priorityEmoji = '';
                if (!(empty(static::$PRIORITY_CODES[$priorityCode]))) {
                    $priorityStringCode = static::$PRIORITY_CODES[$priorityCode];
                    if (!empty(static::$EMOJI[$priorityStringCode])) {
                        $priorityEmoji = ' ' . static::$EMOJI[$priorityStringCode];
                    }
                }
                $sendOneTaskData.= 'Приоритет: ' . $taskToSendItem['priority']['name'] . $priorityEmoji . PHP_EOL;
                $sendOneTaskData.= 'Дата и время постановки: ' . date('d.m.Y H:i:s', strtotime($taskToSendItem['created_on']));
                $sendData[] = $sendOneTaskData;
                if (($i + 1) % 5 == 0) {
                    $sendDataString = implode(PHP_EOL . PHP_EOL, $sendData);
                    $sendData = [];
                    $this->sendMessage($sendDataString, $userId);
                }
            }

            if (!empty($sendData)) {
                $sendDataString = implode(PHP_EOL . PHP_EOL, $sendData);
                $this->sendMessage($sendDataString, $userId);
            }
        }

        public function notifyUsersTasks()
        {
            $usersInfoPath = __DIR__ . '/data/user_settings/';
            $usersInfo = scandir($usersInfoPath);
            $usersInfo = array_filter($usersInfo, function($userInfoFileName) {
                return  preg_match('/.*\.json$/', $userInfoFileName);
            });
            unset($userInfoFileName);
            $usersInfo = array_map(function($userInfoFileName) {
                return str_replace('.json', '', $userInfoFileName);
            }, $usersInfo);
            unset($userInfoFileName);
            foreach ($usersInfo as $userId) {
                $this->sendNewUserTasks($userId);
            }
            unset($userId);
        }

        public function sendNewUserTasks($userId = 0)
        {
            if ($userId === 0) {
                $userId = $this->userId;
            }

            $allTasks = $this->getAllUserTasks($userId);
            $viewedTasks = $this->getData('viewed_tasks/' . $userId);
            if ($viewedTasks) {
                $allTasks = array_filter($allTasks, function($taskItem) use ($viewedTasks) {
                    if (isset($viewedTasks[$taskItem['id']])) {
                        $createdOnTimestamp = strtotime($taskItem['updated_on']);
                        if ($createdOnTimestamp == $viewedTasks[$taskItem['id']]) {
                            return false;
                        }
                    }
                    return true;
                });
                unset($taskItem);

                $allTasks = array_values($allTasks);
            }

            $this->setViewedTasks($allTasks, $userId);

            if (!empty($allTasks)) {
                $this->sendTasks($allTasks, $userId);
            }
        }

        protected function sendAllUserTasks($userId = 0)
        {
            if ($userId === 0) {
                $userId = $this->userId;
            }

            $redmineTasks = $this->getAllUserTasks();

            if (count($redmineTasks) == 0) {
                $this->sendMessage('На данный момент у Вас нет задач.');
                return;
            }

            $this->sendMessage('Все незакрытые задачи:');

            $this->sendTasks($redmineTasks, $userId);

            $this->setViewedTasks($redmineTasks, $userId);
        }

        protected function checkUserSettings($userId = 0)
        {
            if ($userId === 0) {
                $userId = $this->userId;
            }

            return $this->getData('user_settings/' . $userId);
        }
    }
?>