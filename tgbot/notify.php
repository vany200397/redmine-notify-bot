<?php
    define('IMAGES_PATH', __DIR__ . '/images/');
    require_once __DIR__ . '/config.php';
    require_once __DIR__ . '/tgbot.php';
    require_once __DIR__ . '/redmine_notify_tg_bot.php';
    $redmineNotifyTgBot = new RedmineNotifyTgBot();
    $redmineNotifyTgBot->notifyUsersTasks();
    