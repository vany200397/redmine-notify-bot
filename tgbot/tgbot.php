<?php
    class TgBot
    {
        protected static $REQUEST_TOKEN = REQUEST_TOKEN;
        protected static $LOG_PATH = __DIR__ . '/tg_bot.log';
        protected static $TASKS_PATH = __DIR__ . '/tasks/';
        protected  static $API_URL = 'https://api.telegram.org/bot' . TG_BOT_TOKEN . '/';
        protected  static $LOG_SEPARATOR = '---------------------------------------------------';
        protected $lastResponse;
        protected $request;
        protected $keyBoardAutoRemove;
        protected $loggedType;
        protected $userId;
        protected $userMessage;
        public function __construct($request = null, $loggedType = ['response', 'request', 'user_request'], $keyBoardAutoRemove = false)
        {
            $this->lastResponse = [
                'status' => false,
                'data' => '',
                'code' => 0,
                'headers' => [],
                'error_text' => 'No last response'
            ];

            $this->loggedType = $loggedType;

            $this->keyBoardAutoRemove = $keyBoardAutoRemove;

            $this->request = $request;

            $this->userId = null;

            $this->userMessage = null;

            if ($request) {
                $this->processRequest();
            }
        }

        protected function writeLog(string $text, string $type = '', bool $addDataTime = true)
        {
            if (file_exists(self::$LOG_PATH)) {
                $errorStr = '';
                if ($addDataTime) {
                    $errorStr.= '[' . date('d.m.Y H:i:s') . '] ';
                }
                if ($type != '') {
                    $errorStr.= '[' . $type . '] ';
                }
                $errorStr.= $text . PHP_EOL . self::$LOG_SEPARATOR . PHP_EOL . PHP_EOL;
                file_put_contents(self::$LOG_PATH, $errorStr, FILE_APPEND);
            }

            return $this;
        }

        protected function readJson($filePath)
        {
            if (file_exists($filePath)) {
                if ($data = file_get_contents($filePath)) {
                    return json_decode($data, true);
                }
            }
            
            return false;
        }

        protected function getData(string $dataName, string $dataKey = '')
        {
            $fileName = __DIR__ . '/data/' . $dataName . '.json';
            $result = $this->readJson($fileName);
            if ($dataKey && is_array($result)) {
                if (isset($result[$dataKey])) {
                    $result = $result[$dataKey];
                }
            }
            
            return $result;
        }

        public function writeData(string $dataName, $data, $append = true)
        {
            $filePath = __DIR__ . '/data/' . $dataName . '.json';
            
            return $this->writeJson($filePath, $data, $append);
        }

        protected function processRequest()
        {
            if (in_array('user_request', $this->loggedType)) {
                $this->writeLog($this->request, 'user_request');
            }

            $this->request = json_decode($this->request, true);
            $this->userId = $this->request['message']['from']['id'];
            $this->userMessage = $this->request['message']['text'];
        }

        protected function getRequestMessage()
        {
            if ($this->request != null);
        }

        /** @return self */
        protected function setLastResponse(bool $status, $data = '', int $code = 0, array $headers = [], string $errorText = ''): self
        {
            if ($code == 0) {
                if (!$status) {
                    $code = 400;
                } else {
                    $code = 200;
                }
            }
            $this->lastResponse = [
                'status' => $status,
                'data' => $data,
                'code' => $code,
                'headers' => $headers,
                'error_text' => $errorText
            ];
            return $this;
        }

        public function getLastResponse()
        {
            return $this->lastResponse;
        }

        protected function setAppLastResponseError($errorText = '')
        {
            $this->setLastResponse(
                false,
                '',
                0,
                [],
                $errorText
            );
        }

        public function sendRequest(string $url, string $method = 'GET', $data = '', array $headers = [])
        {
            $curl = curl_init($url);
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_HEADER, 1);
            curl_setopt($curl, CURLOPT_NOBODY, 0);
            if (count($headers) > 0) {
                curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
            }
            $logStr = '';
            if (in_array('request', $this->loggedType)) {
                $logRequestHeaders = json_encode($headers);
                $logRequestBody = (is_array($data)) ? json_encode($data) : $data;
                $logStr.= 'Request: ' . PHP_EOL . 'Url: ' . $url . PHP_EOL . 'Method:' . $method . PHP_EOL . 'Headers: ' . $logRequestHeaders . PHP_EOL . 'Body: ' . $logRequestBody . PHP_EOL;
            }
            if (in_array($method, ['POST', 'PUT', 'PATCH'])) {
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            } elseif (!in_array($method, ['DELETE', 'GET', 'HEAD'])) {
                if (in_array('request', $this->loggedType)) {
                    $logStr.= PHP_EOL . 'Not supported request method';
                    $this->writeLog($logStr, 'ERROR REQUEST');
                }
                $this->setAppLastResponseError('TG Bot can send only POST, GET, PUT, PATCH, DELETE, HEAD methods. Method "' . $method . '" not supported');
                return $this;
            }
            $response = curl_exec($curl);
            if ($response) {
                $headerSize = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
                $curlResultHeaders = substr($response, 0, $headerSize);
                $curlResultHeadersArr = explode(PHP_EOL, $curlResultHeaders);
                $curlResultBody = substr($response, $headerSize);
                $responseCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
                $this->setLastResponse(
                    true,
                    $curlResultBody,
                    $responseCode,
                    $curlResultHeadersArr,
                    ''
                );
                if (in_array('request', $this->loggedType)) {
                    $logStr.=
                        PHP_EOL . 'Response:' . PHP_EOL .
                        'Headers: ' . $curlResultHeaders . PHP_EOL .
                        'HTTP Code: ' . $responseCode . PHP_EOL .
                        'Body: ' . $curlResultBody
                    ;
                    $this->writeLog($logStr, 'RESPONSE');
                }
            } else {
                $this->setAppLastResponseError('Unknown response error');
            }

            return $this;
        }

        public function sendMessage(string $message, int $to = 0, array $inlineKeyboard = [], array $keyboard = [])
        {
            if ($to === 0) {
                $to = $this->userId;
            } 

            $sendData = [
                'chat_id' => $to,
                'text' => $message,
                'parse_mode' => 'html'
            ];

            if (count($inlineKeyboard) != 0) {
                $inlineKeyboard = [
                    'inline_keyboard' => $inlineKeyboard
                ];
                $sendData['reply_markup'] = json_encode($inlineKeyboard);
            }

            $request = $this->sendRequest(
                self::$API_URL . 'sendMessage',
                'POST',
                $sendData
            );
        }

        protected function writeJson(string $filePath, $data, $append = true, $replaceLastId = false)
        {
            if (!is_array($data)) {
                $data = json_decode($data, true);
            }
            if (!$data) {
                return;
            }
            $lastId = 0;
            $contentToWrite = '';
            if (file_exists($filePath)) {
                if ($append) {
                    $jsonContent = file_get_contents($filePath);
                    $decodeJsonContent = json_decode($jsonContent, true);
                    if (!$decodeJsonContent) {
                        $decodeJsonContent = [];
                    } else {
                        $prevLastDataItem = (int)$decodeJsonContent[array_key_last($decodeJsonContent)]['id'];
                        $lastId = $prevLastDataItem + 1;   
                    }
                    $decodeJsonContent = array_merge($decodeJsonContent, $data);
                    $contentToWrite = json_encode($decodeJsonContent, JSON_PRETTY_PRINT);
                } else {
                    $contentToWrite = json_encode($data, JSON_PRETTY_PRINT);
                }
            } else {
                $contentToWrite = json_encode($data, JSON_PRETTY_PRINT);
            }

            if ($replaceLastId) {
                $contentToWrite = str_replace('#_LAST_ID_#', $lastId, $contentToWrite);
            }

            return (file_put_contents($filePath, $contentToWrite) !== false) ? true : false;
        }

        public function writeTask(string $task = '', int $userId = 0)
        {
            if ($userId == 0) {
                $userId = $this->userId;
            }
            $taskFile = static::$TASKS_PATH . $userId . '.json';
            $taskFileContent[] = [
                'id' => '#_LAST_ID_#',
                'date' => time(),
                'task' => $task
            ];
            
            return $this->writeJson($taskFile, $taskFileContent, true, true);
        }

        public function getTasks($userId = 0)
        {
            if ($userId === 0) {
                $userId = $this->userId;
            }

            $filePath = static::$TASKS_PATH . $userId . '.json';
            
            return $this->readJson($filePath);
        }

        public function getLastTask($userId = 0)
        {
            if ($userId === 0) {
                $userId = $this->userId;
            }

            $tasks = $this->getTasks($userId);

            if ($tasks) {
                return array_pop($tasks);
            }

            return false;
        }

        public function sendPicture(string $fileName, string $caption = '', int $to = 0)
        {

            if ($to === 0) {
                $to = $this->userId;
            }

            $curlFile = new \CURLFile($fileName);

            $sendData = [
                'chat_id' => $to,
                'photo' => $curlFile
            ];

            if ($caption != '') {
                $sendData['caption'] = $caption;
            }

            $this->sendRequest(
              self::$API_URL . 'sendPhoto',
              'POST',
              $sendData
            );
        }
    }